<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class JobController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// This is the job listing
	}

	public function View($jobId, $jobName) {
		// View a single job listing
	}

	public function profileJobs()
	{
		// List all of the jobs for a given user
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// create a new job listing
	}

}
