<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class JobModel extends Model {

    protected $table = 'jobs';
    protected $fillable = array('fk_user', 'title', 'description');

}
